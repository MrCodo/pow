package pow

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
)

// Create spiking neuron and make it spike by giving it two stimulations.
func TestOneNeuron(t *testing.T) {
	out := make(chan int)
	sn := NewSpikingNeuron(1, out)
	defer sn.Stop()

	sn.Stimulate(400)
	sn.Stimulate(700)

	id := <-out
	fmt.Println("id", id)
	if id != sn.id {
		t.Fatal("not getting correct id from spiking neuron")
	}
}

// Create two linked neurons. Make first neuron spike a few times,
// which triggers a spike of the second neuron.
func TestTwoNeurons(t *testing.T) {
	sn1 := NewSpikingNeuron(1, nil)
	defer sn1.Stop()
	fmt.Println("id sn1", sn1.id)

	out := make(chan int)
	sn2 := NewSpikingNeuron(2, out)
	defer sn2.Stop()
	fmt.Println("id sn2", sn2.id)

	err := sn1.AddNeighbor(sn2, 400)
	if err != nil {
		t.Fatal(err)
	}

	sn1.Stimulate(spikingThreshold)
	sn1.Stimulate(spikingThreshold)
	sn1.Stimulate(spikingThreshold)

	id := <-out
	fmt.Println("out id", id)
	if id != sn2.id {
		t.Fatal("not expected id")
	}
}

func TestUnmarshalNeuron(t *testing.T) {
	neuronJSON := []byte(`{"id": 1,"synapses":[{"neighbor":2,"strength":400}]}`)
	sn := NewSpikingNeuron(1, nil)
	defer sn.Stop()
	err := json.Unmarshal(neuronJSON, &sn)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(sn.String())
}

func TestMarshalBrain(t *testing.T) {
	sn1 := NewSpikingNeuron(1, nil)
	sn2 := NewSpikingNeuron(2, nil)
	sn3 := NewSpikingNeuron(3, nil)

	err := sn1.AddNeighbor(sn2, 400)
	if err != nil {
		t.Fatal(err)
	}
	err = sn1.AddNeighbor(sn3, 400)
	if err != nil {
		t.Fatal(err)
	}

	b := NewBrain()
	defer b.Stop()
	b.AddNeuron(sn1)
	b.AddNeuron(sn2)
	b.AddNeuron(sn3)

	m, err := json.Marshal(b)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(string(m))
}

func TestUnmarshalBrain(t *testing.T) {
	brainJSON := []byte(`{"neurons":{"0":{"id":0,"synapses":[{"neighbor":1,"strength":1000},{"neighbor":2,"strength":1000}]},"1":{"id":1},"2":{"id":2}}}`)
	b := NewBrain()
	defer b.Stop()
	err := json.Unmarshal(brainJSON, &b)
	if err != nil {
		t.Fatal(err)
	}
	b.neurons[0].Stimulate(spikingThreshold)
	id := <-b.out
	fmt.Println("out id", id)
	id = <-b.out
	fmt.Println("out id", id)
	id = <-b.out
	fmt.Println("out id", id)
}

func TestANDGate(t *testing.T) {
	brainJSON := []byte(`{"neurons":{"0":{"id":0,"synapses":[{"neighbor":2,"strength":500}]},"1":{"id":1,"synapses":[{"neighbor":2,"strength":500}]},"2":{"id":2}}}`)
	b := NewBrain()
	defer b.Stop()
	err := json.Unmarshal(brainJSON, &b)
	if err != nil {
		t.Fatal(err)
	}
	b.neurons[0].Stimulate(spikingThreshold)
	b.neurons[1].Stimulate(spikingThreshold)

	// out should see neurons 0, 1 and 2 spike
	spiked := []int{}
	for {
		spiked = append(spiked, <-b.out)
		if len(spiked) == 3 {
			break
		}
	}
	if !reflect.DeepEqual(spiked, []int{0, 1, 2}) {
		t.Fatal("not expected spikes")
	}
}
