package pow

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"
)

// Spiking threshold, energy at which point a neuron spikes.
const spikingThreshold int = 1000

// tLimit is the time limit for communication to / from a neuron.
const tLimit = 10 * time.Millisecond

// SpikingNeuron is a neuron which spikes when its energy reaches a set
// threshold. Each neuron can be uniquely identified.
type SpikingNeuron struct {
	// ID is the neuron's unique identifier.
	id int

	// energy is the neuron's electrical potential.
	energy int

	// charge receives an electric charge from external entities.
	charge chan int

	// neighbors are linked neurons which will receive stimulation
	// after a spike.
	synapses []synapse

	// out allows an external entity to be informed when neuron spikes.
	out chan int

	// done is used to properly stop the neuron.
	done chan struct{}
}

// synapse allows the stimulation of a neighbor neuron.
type synapse struct {
	// neighborID is the identifier of the neighbor neuron receiving the
	// stimulation.
	neighborID int

	// strength indicates the energy transferred through the synapse
	// after a spike.
	strength int

	// charge allows passing of energy to neighbor neuron.
	charge chan int
}

// NewSpikingNeuron returns a new spiking neuron with no energy. It also
// starts the neuron.
func NewSpikingNeuron(id int, out chan int) *SpikingNeuron {
	sn := &SpikingNeuron{
		id:       id,
		energy:   0,
		charge:   make(chan int),
		synapses: []synapse{},
		done:     make(chan struct{}),
		out:      out,
	}
	go sn.main()
	return sn
}

// main is where most of the spiking neuron's behavior is set.
func (sn *SpikingNeuron) main() {
	fmt.Printf("neuron %d started\n", sn.id)
	defer func() {
		sn.done <- struct{}{}
	}()

	for {
		select {
		case <-sn.done:
			return
		case delta := <-sn.charge:
			sn.energy += delta
			if sn.energy >= spikingThreshold {
				sn.spike()
			}
		}
	}
}

// Stop cleanly stops the neuron.
func (sn *SpikingNeuron) Stop() {
	sn.done <- struct{}{}
	<-sn.done
	fmt.Printf("neuron %d stopped\n", sn.id)
}

// String provides a string representation of the spiking neuron.
func (sn *SpikingNeuron) String() string {
	synapses := []string{}
	for _, synapse := range sn.synapses {
		synapses = append(synapses, synapse.String())
	}
	return fmt.Sprintf("ID: %d, Synapses: [%s]", sn.id, strings.Join(synapses, ","))
}

// spike propages energy to other neurons and resets the energy to zero.
func (sn *SpikingNeuron) spike() {
	// advertise spike event to external entity.
	go func() {
		if sn.out != nil {
			sn.out <- sn.id
		}
	}()

	// propagate energy to neighbors.
	var wg sync.WaitGroup
	for _, s := range sn.synapses {
		wg.Add(1)
		go func(s synapse) {
			defer wg.Done()
			stimulate(s.charge, s.strength)
		}(s)
	}
	wg.Wait()

	sn.energy = 0
}

// stimulate tries to increase energy, abort if neuron isn't responding
// fast enough.
func stimulate(charge chan int, strength int) {
	ctx, cancel := context.WithTimeout(context.Background(), tLimit)
	defer cancel()

	select {
	case charge <- strength:
	case <-ctx.Done():
	}
}

// Stimulate allows an external entity to increase the neuron's energy.
func (sn *SpikingNeuron) Stimulate(delta int) {
	// An external entity must increase the energy.
	if delta <= 0 {
		return
	}

	stimulate(sn.charge, delta)
}

// AddNeighbor adds a neighbor neuron to the current neuron with a
// specified link strenght which dictates the amount of stimulation
// which the neighbor will receive after a spike.
func (sn *SpikingNeuron) AddNeighbor(n *SpikingNeuron, strength int) error {
	if strength <= 0 {
		return fmt.Errorf("strenght must be greater than 0")
	}

	synapse := synapse{
		neighborID: n.id,
		strength:   strength,
		charge:     n.charge,
	}
	sn.synapses = append(sn.synapses, synapse)

	return nil
}

// MarshalJSON allows the marshalling of unexported spiking neuron fields.
func (sn *SpikingNeuron) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		ID       int       `json:"id"`
		Synapses []synapse `json:"synapses,omitempty"`
	}{
		ID:       sn.id,
		Synapses: sn.synapses,
	})
}

// UnmarshalJSON allows the unmarshalling of a skiping neuron.
func (sn *SpikingNeuron) UnmarshalJSON(b []byte) error {
	neuron := struct {
		ID       int       `json:"id"`
		Synapses []synapse `json:"synapses,omitempty"`
	}{}
	err := json.Unmarshal(b, &neuron)
	if err != nil {
		return err
	}
	sn.id = neuron.ID
	sn.synapses = neuron.Synapses
	return nil
}

// MarshalJSON allows the marshalling of unexported synapse fields.
func (s *synapse) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		NeighborID int `json:"neighbor"`
		Strength   int `json:"strength"`
	}{
		NeighborID: s.neighborID,
		Strength:   s.strength,
	})
}

// UnmarshalJSON allows the unmarshalling of a synapse.
func (s *synapse) UnmarshalJSON(b []byte) error {
	synapse := struct {
		NeighborID int `json:"neighbor"`
		Strength   int `json:"strength"`
	}{}
	err := json.Unmarshal(b, &synapse)
	if err != nil {
		return err
	}
	s.neighborID = synapse.NeighborID
	s.strength = synapse.Strength
	s.charge = nil
	return nil
}

// String provides a string representation of the synapse.
func (s *synapse) String() string {
	return fmt.Sprintf("NeighborID: %d, Strength: %d", s.neighborID, s.strength)
}

// Brain is a neural network containing multiple spiking neurons.
type Brain struct {
	// neurons are the working components of the brain.
	neurons map[int]*SpikingNeuron

	// out monitors some or all spiking neurons of the brain.
	out chan int
}

// NewBrain creates a new empty brain.
func NewBrain() *Brain {
	return &Brain{
		neurons: map[int]*SpikingNeuron{},
		out:     make(chan int),
	}
}

// Stop cleanly stops the brain.
func (b *Brain) Stop() {
	var wg sync.WaitGroup
	for _, sn := range b.neurons {
		wg.Add(1)
		go func(sn *SpikingNeuron) {
			defer wg.Done()
			sn.Stop()
		}(sn)
	}
	wg.Wait()
}

// Reset resets all neurons energy to zero.
func (b *Brain) Reset() {
	for _, sn := range b.neurons {
		sn.energy = 0
	}
}

// String provides a string representation of the brain.
func (b *Brain) String() string {
	neurons := []string{}
	for _, sn := range b.neurons {
		neurons = append(neurons, sn.String())
	}
	return strings.Join(neurons, ",")
}

// AddNeuron adds the passed neuron to the brain.
func (b *Brain) AddNeuron(n *SpikingNeuron) {
	b.neurons[n.id] = n
}

// MarshalJSON allows the marshalling of unexported brain fields.
func (b *Brain) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Neurons map[int]*SpikingNeuron `json:"neurons,omitempty"`
	}{
		Neurons: b.neurons,
	})
}

// UnmarshalJSON allows the unmarshalling of a brain.
func (b *Brain) UnmarshalJSON(bb []byte) error {
	brain := struct {
		Neurons map[int]*SpikingNeuron `json:"neurons,omitempty"`
	}{}
	err := json.Unmarshal(bb, &brain)
	if err != nil {
		return err
	}

	// create and start all neurons
	for _, bn := range brain.Neurons {
		sn := NewSpikingNeuron(bn.id, b.out)
		b.neurons[sn.id] = sn
	}

	// add synapses to all neurons
	for snid, bn := range brain.Neurons {
		for _, synapse := range bn.synapses {
			err := b.neurons[snid].AddNeighbor(b.neurons[synapse.neighborID], synapse.strength)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
